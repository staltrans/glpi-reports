<?php
/**
 *  @version    $Id$
 *  @package    reports
 *  @author     boris_t <boris@talovikov.ru>
 *  @license    https://opensource.org/licenses/GPL-3.0
 *  @link       https://bitbucket.org/staltrans/glpi-reports
 */

/**
 * User selection criteria
 */
class PluginReportsUserCriteriaCustom extends PluginReportsDropdownCriteria {

  /**
   *  @see  User::dropdown();
   *  @link https://github.com/glpi-project/glpi/blob/master/inc/user.class.php
   */
  private $options = array();

  /**
   * @param $report
   * @param $name
   * @param $label        (default '')
   * @param $condition    (default '')
   */
  function __construct($report, $name='user', $label='', $condition='') {
    parent::__construct($report, $name, 'glpi_users', ($label ? $label : _n('User', 'Users', 1)), $condition);
    $this->options = array(
      'name'     => $this->getName(),
      'value'    => $this->getParameterValue(),
      'comments' => $this->getDisplayComments(),
      'entity'   => $this->getEntityRestrict(),
      'right'    => 'all',
    );
  }

  public function displayDropdownCriteria() {
    User::dropdown($this->options);
  }

  public function setOptions($opt) {
    $this->options = array_merge($this->options, $opt);
  }

}
