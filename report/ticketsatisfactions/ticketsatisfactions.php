<?php
/**
 *  @version    $Id$
 *  @package    reports
 *  @author     boris_t <boris@talovikov.ru>
 *  @license    https://opensource.org/licenses/GPL-3.0
 *  @link       https://bitbucket.org/staltrans/glpi-reports
 */

$USEDBREPLICATE         = 1;
$DBCONNECTION_REQUIRED  = 0;

include ("../../../../inc/includes.php");

$report = new PluginReportsAutoReport(__('ticketsatisfactions_report_title', 'reports'));

$date = new PluginReportsDateIntervalCriteria($report);

$now = new DateTime();
$startdate = $now->format('Y-m-01');
$month = new DateInterval('P1M');
$enddate = new DateTime($startdate);
$enddate->add($month);

$date->setStartDate($startdate);
$date->setEndDate($enddate->format('Y-m-d'));

$report->displayCriteriasForm();

if ($report->criteriasValidated()) {

  try {
    $startdate = new DateTime($date->getStartDate());
    $enddate = new DateTime($date->getEndDate());
    $date_range = " t.date >= '" . $startdate->format('Y-m-d H:i:s') . "' AND t.date <= '" . $enddate->format('Y-m-d H:i:s') . "' ";
  } catch (Exception $e) {
    echo "Error: $e->getMessage()<br />";
  }

  $report->setSubNameAuto();
  $report->setColumns(array(
    new PluginReportsColumnLink('user', _n('User', 'User', 1), 'User'),
    new PluginReportsColumn('created', _n('Created', 'Created', 1)),
    new PluginReportsColumn('evaluated', _n('Evaluated', 'Evaluated', 1)),
  ));

  $query = "SELECT t.users_id_recipient as user,
                   count(t.id) as created,
                   count(ts.satisfaction) as evaluated
            FROM glpi_tickets t
            LEFT JOIN glpi_ticketsatisfactions ts ON ts.tickets_id = t.id
            WHERE t.is_deleted='0' " .
            (isset($date_range) ? "AND $date_range" : "") .
            " GROUP BY t.users_id_recipient ORDER BY evaluated DESC";
   $report->setSqlRequest($query);
   $report->execute();
}
