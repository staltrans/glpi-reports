<?php
/**
 *  @version    $Id$
 *  @package    reports
 *  @author     boris_t <boris@talovikov.ru>
 *  @license    https://opensource.org/licenses/GPL-3.0
 *  @link       https://bitbucket.org/staltrans/glpi-reports
 */

$USEDBREPLICATE         = 1;
$DBCONNECTION_REQUIRED  = 0;

include ("../../../../inc/includes.php");

$report = new PluginReportsAutoReport(__('worktimebyusers_report_title', 'reports'));

$date = new PluginReportsDateIntervalCriteria($report);

$now = new DateTime();
$startdate = $now->format('Y-m-01');
$month = new DateInterval('P1M');
$enddate = new DateTime($startdate);
$enddate->add($month);

$date->setStartDate($startdate);
$date->setEndDate($enddate->format('Y-m-d'));

$users = new PluginReportsUserCriteriaCustom($report);
$users->setOptions(array('right' => 'ticketcost'));
$users->setSqlField("glpi_users.id");

$report->displayCriteriasForm();

$uid = $users->getParameterValue();

if ($report->criteriasValidated() && isset($uid) && is_numeric($uid)) {

  try {
    $startdate = new DateTime($date->getStartDate());
    $enddate = new DateTime($date->getEndDate());
    $date_range = " t.date >= '" . $startdate->format('Y-m-d H:i:s') . "' and t.date <= '" . $enddate->format('Y-m-d H:i:s') . "' ";
  } catch (Exception $e) {
    echo "Error: $e->getMessage()<br />";
  }

  $report->setSubNameAuto();
  $report->setColumns(array(
    new PluginReportsColumn('closedelay', _n('Close Delay', 'Clode Delay', 1)),
    new PluginReportsColumn('solvedelay', _n('Solve Delay', 'Solve Delay', 1)),
    new PluginReportsColumn('takeintoaccountdelay', _n('Take In To Account Delay', 'Take In To Account Delay', 1)),
    new PluginReportsColumn('waitingduration', _n('Waiting Duration', 'Waiting Duration', 1)),
    new PluginReportsColumnLink('cid', _n('Category', 'Category', 1), 'ITILCategory'),
  ));

  $query = "SELECT round(sum(t.close_delay_stat)/3600, 2) as closedelay,
                   round(sum(t.solve_delay_stat)/3600, 2) as solvedelay,
                   round(sum(t.takeintoaccount_delay_stat)/3600, 2) as takeintoaccountdelay,
                   round(sum(t.waiting_duration)/3600, 2) as waitingduration,
                   c.id as cid
            FROM  glpi_tickets_users tu
            LEFT JOIN glpi_tickets t ON tu.tickets_id=t.id
            LEFT JOIN glpi_users u ON tu.users_id=u.id
            LEFT JOIN glpi_itilcategories c ON c.id=t.itilcategories_id
            WHERE t.is_deleted='0' and tu.type='2' AND u.id='$uid'" .
            (isset($date_range) ? "AND $date_range" : "") .
            " GROUP BY t.itilcategories_id";

   $report->setSqlRequest($query);
   $report->execute();
}
