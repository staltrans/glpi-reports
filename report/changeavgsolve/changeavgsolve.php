<?php
/**
 *  @version    $Id$
 *  @package    reports
 *  @author     boris_t <boris@talovikov.ru>
 *  @license    https://opensource.org/licenses/GPL-3.0
 *  @link       https://bitbucket.org/staltrans/glpi-reports
 */

$USEDBREPLICATE         = 1;
$DBCONNECTION_REQUIRED  = 0;

include ("../../../../inc/includes.php");

$report = new PluginReportsAutoReport(__('changeavgsolve_report_title', 'reports'));

$date = new PluginReportsDateIntervalCriteria($report);

$now = new DateTime();
$startdate = $now->format('Y-m-01');
$month = new DateInterval('P1M');
$enddate = new DateTime($startdate);
$enddate->add($month);

$date->setStartDate($startdate);
$date->setEndDate($enddate->format('Y-m-d'));

$report->displayCriteriasForm();

if ($report->criteriasValidated()) {

  try {
    $startdate = new DateTime($date->getStartDate());
    $enddate = new DateTime($date->getEndDate());
    $date_range = " c.closedate >= '" . $startdate->format('Y-m-d H:i:s') . "' and c.closedate <= '" . $enddate->format('Y-m-d H:i:s') . "' ";
  } catch (Exception $e) {
    echo "Error: $e->getMessage()<br />";
  }

  $report->setSubNameAuto();
  $report->setColumns(array(
    new PluginReportsColumn('avgtime', _n('Average time', 'Average time', 1)),
    new PluginReportsColumnLink('cid', _n('Category', 'Category', 1), 'ITILCategory'),
  ));

  $query = "SELECT round(avg(solve_delay_stat)/60, 0) AS avgtime,
                   i.id as cid
            FROM glpi_changes c
            LEFT JOIN glpi_itilcategories i ON c.itilcategories_id=i.id
            WHERE c.is_deleted='0'" .
            (isset($date_range) ? "AND $date_range" : "") .
            " GROUP BY cid";
   $report->setSqlRequest($query);
   $report->execute();
}
